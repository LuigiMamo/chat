package com.chat.chatGroup.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="messaggi")
public class Messaggio {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "messaggio_id")
	private int id;
	
	@GeneratedValue
	@Column(insertable = false)
	private String data_messaggio;
	@Column
	private String username;
	@Column 
	private String messaggio;
	
	public Messaggio() {
		
	}
	
	public Messaggio(int id, String data_messaggio, String username, String messaggio) {
		super();
		this.id = id;
		this.data_messaggio = data_messaggio;
		this.username = username;
		this.messaggio = messaggio;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getData_messaggio() {
		return data_messaggio;
	}
	public void setData_messaggio(String data_messaggio) {
		this.data_messaggio = data_messaggio;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getMessaggio() {
		return messaggio;
	}
	public void setMessaggio(String messaggio) {
		this.messaggio = messaggio;
	}
	
	
	

}
