package com.chat.chatGroup.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chat.chatGroup.models.Messaggio;
import com.chat.chatGroup.repositories.DataAccessRepo;

@Service
public class chatService implements DataAccessRepo<Messaggio> {

	@Autowired
	private EntityManager entMan;
	
	private Session getSessione() {
		return entMan.unwrap(Session.class);
	}

	@Override
	public Messaggio insert(Messaggio t) {

		Messaggio temp = new Messaggio();
		temp.setUsername(t.getUsername());
		temp.setMessaggio(t.getMessaggio());
		
		Session sessione = this.getSessione();
		try {
			sessione.save(temp);
			return temp;
			
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@Override
	public List<Messaggio> findAll() {
		
		Session sessione = getSessione();
		return sessione.createCriteria(Messaggio.class).list();
		
	}
	
	
		
	
}
