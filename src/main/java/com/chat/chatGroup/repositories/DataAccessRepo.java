package com.chat.chatGroup.repositories;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface DataAccessRepo<T> {

	T insert(T t);
	List<T> findAll();
}
