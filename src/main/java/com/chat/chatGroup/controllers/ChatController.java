package com.chat.chatGroup.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chat.chatGroup.models.Messaggio;
import com.chat.chatGroup.services.chatService;

@RestController
@RequestMapping("/chat")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ChatController {
	
	@Autowired
	private chatService service;

	@PostMapping("/insert")
	public Messaggio insert(@RequestBody Messaggio varChat) {
		return service.insert(varChat);
	}
	
	@GetMapping("/list")
	public List<Messaggio> allChat(){
		return service.findAll();
	}
}
